<?php

namespace AddressSuggester;

use Curl\Curl;

/**
 * Class Suggester
 * @package AddressSuggester
 * @author Andrea Kuciakova <andrea.kuciakova@gmail.com>
 */
class Suggester {

    const API = "http://autocomplete.geocoder.api.here.com/6.2/suggest.json";

    /** @var string */
    private $appId;

    /** @var string */
    private $appCode;

    /** @var Curl */
    private $curl;

    /**
     * Suggester constructor.
     * @param string $appId
     * @param string $appCode
     */
    public function __construct(string $appId, string $appCode) {
        $this->appId = $appId;
        $this->appCode = $appCode;
        $this->curl = new Curl();
    }

    /**
     * Suggest address by query
     * @param string $query
     * @return Address[]
     */
    public function suggest(string $query) : array {
        $this->curl->setUrl(self::API, [
            'app_id' => $this->appId,
            'app_code' => $this->appCode,
            'query' => $query,
            'country' => 'CZE'
        ]);
        $response = $this->curl->exec();
        $addresses = [];
        if (isset($response->suggestions) && is_array($suggestions = $response->suggestions)) {
            foreach($suggestions as $suggestion) {
                if ($suggestion->address) {
                    $addresses[] = Address::create($suggestion->address);
                }
            }
        }
        return $addresses;
    }
}