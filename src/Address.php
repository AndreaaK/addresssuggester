<?php

namespace AddressSuggester;

/**
 * Class Address
 * @package AddressSuggester
 * @author Andrea Kuciakova <andrea.kuciakova@gmail.com>
 */
class Address {

    /** @var string */
    private $street;

    /** @var string */
    private $houseNumber;

    /** @var string */
    private $city;

    /** @var string */
    private $postalCode;

    /** @var string */
    private $district;

    /** @var string */
    private $country;

    /** @var string */
    private $state;

    /** @var string */
    private $county;

    /**
     * @return string
     */
    public function getStreet(): ?string {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getHouseNumber(): ?string {
        return $this->houseNumber;
    }

    /**
     * @return string
     */
    public function getCity(): ?string {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getPostalCode(): ?string {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getDistrict(): ?string {
        return $this->district;
    }

    /**
     * @return string
     */
    public function getCountry(): ?string {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getState(): ?string {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getCounty(): ?string {
        return $this->county;
    }

    /**
     * Create Address object from stdClass
     * @param \stdClass $suggestion
     * @return static
     */
    public static function create(\stdClass $suggestion) {
        $address = new static();
        $reflection = new \ReflectionClass($address);
        foreach($reflection->getProperties() as $property) {
            $name = $property->getName();
            $property->setAccessible(TRUE);
            if (isset($suggestion->$name)) {
                $property->setValue($address, $suggestion->$name);
            }
        }
        return $address;
    }
}